package com.company;

public class SuperException extends Exception {
    public SuperException(String message) {
        super(message);
    }
}
