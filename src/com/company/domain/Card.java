package com.company.domain;

import com.company.enums.CardType;
import com.company.enums.CardValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Card implements Comparable<Card>{

    /**
     * Масть
     */
    private CardType type;

    /**
     * Значение
     */
    private CardValue value;

    @Override
    public int compareTo(Card o) {
        return this.value.compareTo(o.value);
    }
}
