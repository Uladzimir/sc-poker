package com.company.domain;

import com.company.enums.CardType;
import com.company.enums.CardValue;

import java.util.ArrayList;
import java.util.List;

public class Deck {


    private List<Card> items;

    public Deck() {
        items = new ArrayList<>();

        CardType[] types = CardType.values();

        CardValue[] values = CardValue.values();

        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < types.length; j++) {
                Card card = new Card();
                card.setType(types[j]);
                card.setValue(values[i]);

                items.add(card);
            }
        }
    }

    public List<Card> getItems() {
        return items;
    }
}
