package com.company.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Set;

@Data
public class Person {
    private String name;

    private BigDecimal money;

    private Set<Card> cards;

    private boolean isDealer = false;
    private boolean isBigBlind = false;
    private boolean isSmallBlind = false;
    private boolean inGame = true;

//    public boolean isCall() {
//
//    }
}
