package com.company.domain;

import com.company.util.RandomValue;
import com.company.util.RandomValueImpl;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

@Data
public class Table {
    private List<Person> people;

    private BigDecimal bank = BigDecimal.ZERO;

    private Deck deck;

    private Set<Card> commonCards;

    public Table(int count) {
        people = new ArrayList<>(count);

        RandomValue randomValue = new RandomValueImpl();

        HashSet<String> set = randomValue.getUniqueNames(count);

        deck = new Deck();

        shuffleDeck();

        for (String name : set) {
            Person person = new Person();
            person.setName(name);
            person.setMoney(new BigDecimal("100.00"));
            Set<Card> personCards = new HashSet<>(2);

            for (int i = 0; i < 2; i++) {
                personCards.add(deck.getItems().remove(0));
            }

            person.setCards(personCards);
            people.add(person);
        }



        setRoles();

    }

    private void shuffleDeck() {
        Date date = new Date();
        Random rnd = new Random(date.getTime());
        Collections.shuffle(deck.getItems(), rnd);
    }

    private void setRoles() {
        people.get(0).setDealer(true);
        people.get(1).setSmallBlind(true);
        people.get(2).setBigBlind(true);
    }
}
