package com.company.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Game {

    private BigDecimal currentBid = new BigDecimal("10.00");

    public void start(Table table) {
        List<Person> people = table.getPeople();

        Person personWithSmallBlind = people.get(1);

        BigDecimal value = currentBid.divide(new BigDecimal("2.00"));

        personWithSmallBlind
                .setMoney(personWithSmallBlind.getMoney()
                        .subtract(value));

        table.setBank(value);

        Person personWithBigBlind = people.get(2);

        value = currentBid;

        personWithBigBlind
                .setMoney(personWithBigBlind.getMoney()
                        .subtract(value));

        table.setBank(table.getBank().add(value));

        Date date = new Date();
        Random rnd = new Random(date.getTime());

        for (Person person : people) {
            if (!person.isBigBlind()) {

                boolean inGame = rnd.nextBoolean();
                if (inGame) {
                    if(person.isSmallBlind()) {
                        value = currentBid.divide(new BigDecimal("2.00"));
                        person.setMoney(person.getMoney()
                                .subtract(value));
                        table.setBank(table.getBank().add(value));
                    } else {
                        value = currentBid;
                        person.setMoney(person.getMoney()
                                .subtract(value));
                        table.setBank(table.getBank().add(value));
                    }
                }
                person.setInGame(inGame);
            }
        }

        for (Person person : people) {
            System.out.println(person);
        }

        System.out.println(table.getBank());

    }
}
