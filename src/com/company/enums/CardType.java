package com.company.enums;

public enum CardType {
    DIAMONDS,
    HEARTS,

    /**
     * Пики
     */
    SPADES,
    CLUBS
}
