package com.company;

import org.junit.Test;

public class TestException {

    private int divide(int a, int b) {
        return a / b;
    }


    @Test
    public void testMain() {

        Integer c = null;

        try {
            c = divide(5, 0);
        } catch (ArithmeticException e) {
            e.printStackTrace();
            c = 0;
        }

        System.out.println("Программа успешно завершена");

        System.out.println(c);
    }
}
