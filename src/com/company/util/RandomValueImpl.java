package com.company.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class RandomValueImpl implements RandomValue {

    private static final String[] NAMES = {
            "Марко Поло",
            "Христофор Колумб",
            "Александр Пушник",
            "Тадэуш Кастюшка",
            "Стив Джобс",
            "Илон Маск"
    };
    private static final Random rand = new Random();

    public HashSet<String> getUniqueNames(int count) {
        HashSet<String> set = new HashSet<>(count);

        while (set.size() < count) {
            set.add(NAMES[rand.nextInt(NAMES.length)]);
        }

        return set;
    }
}
