package com.company.util;

import java.util.HashSet;
import java.util.Set;

public interface RandomValue {
    HashSet<String> getUniqueNames(int count);
}
