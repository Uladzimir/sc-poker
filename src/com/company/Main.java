package com.company;

import com.company.domain.Game;
import com.company.domain.Table;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Введите количество игроков: ");
        Scanner in = new Scanner(System.in);

        Integer count = null;

        try {
            count = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Введённые данные не являются числом");
            System.exit(1);
        }

        Table table = new Table(count);

        Game game = new Game();
        game.start(table);//

    }


}
