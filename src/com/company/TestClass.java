package com.company;

import com.company.domain.Card;
import com.company.domain.Person;
import com.company.enums.CardType;
import com.company.enums.CardValue;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class TestClass {

    @Test
    public void testCard() {
        Card card = new Card();

        Assert.assertNotNull(card);

        card.setValue(CardValue.ACE);

        card.setType(CardType.DIAMONDS);

        Assert.assertEquals(card.getValue(), CardValue.ACE);

        System.out.println(card);
    }

    @Test
    public void testEnum() {
        System.out.println(Arrays.toString(CardType.values()));
    }

    @Test
    public void fillDeck() {
        Set<Card> set = new HashSet<>();

        CardType[] types = CardType.values();

        CardValue[] values = CardValue.values();

        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < types.length; j++) {
                Card card = new Card();
                card.setType(types[j]);
                card.setValue(values[i]);

                set.add(card);
            }
        }

        for (Card card : set) {
            System.out.println(card);
        }

    }

    @Test
    public void testMoney() {

        BigDecimal currentBid = new BigDecimal("10.00");

        Person personWithSmallBlind = new Person();

        personWithSmallBlind.setMoney(new BigDecimal("100.00"));

        personWithSmallBlind
                .setMoney(personWithSmallBlind.getMoney()
                        .subtract(currentBid.divide(new BigDecimal("2.00"))));

        Assert.assertEquals(personWithSmallBlind.getMoney(), new BigDecimal("95.00"));
    }

    @Test
    public void testBoolean() {
//        for (int i = 0; i < 10 ; i++) {
//            System.out.println(Math.random() < 0.5);
//        }

        Date date = new Date();
        Random rnd = new Random(date.getTime());

        for (int i = 0; i < 10; i++) {
            System.out.println(rnd.nextBoolean());
        }


    }
}
