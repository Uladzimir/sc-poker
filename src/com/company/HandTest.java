package com.company;

import com.company.domain.Card;
import com.company.domain.Person;
import com.company.enums.CardType;
import com.company.enums.CardValue;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class HandTest {

    @Test
    public void royalFlushTest() throws Exception {

        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.FOUR),
                        new Card(CardType.HEARTS, CardValue.JACK)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.CLUBS, CardValue.JACK)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.CLUBS, CardValue.ACE)
                )
        );

        Set<Card> result = joinCollections(person.getCards(), commonCards);

        Assert.assertTrue(isNotRoyalFlush(result));

        result = joinCollections(person.getCards(), commonCards2);

        boolean isNotRoyalFlush = isNotRoyalFlush(result);

        Assert.assertFalse(isNotRoyalFlush);

        if (!isNotRoyalFlush) {
            if (isStraightFlush(result)) {

            }
        }
    }

    private boolean isNotRoyalFlush(Set<Card> commonCards) {

        for (Card card : commonCards) {
            if (card.getValue().equals(CardValue.ACE)) {
                return false;
            }
        }

        return true;
    }

    @Test
    public void flushTest() {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.FOUR),
                        new Card(CardType.HEARTS, CardValue.JACK)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.DIAMONDS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.CLUBS, CardValue.ACE)
                )
        );

        commonCards.addAll(person.getCards());

        Set<Card> cards = getFlushSet(commonCards);

        Assert.assertTrue(!cards.isEmpty());

        commonCards2.addAll(person.getCards());

        cards = getFlushSet(commonCards2);

        Assert.assertTrue(cards.isEmpty());
    }

    private Set<Card> joinCollections(Set<Card> set1, Set<Card> set2) throws Exception {

        int size = set1.size() + set2.size();

        Set<Card> resultCards = new HashSet<>(set1);
        resultCards.addAll(set2);

        if (resultCards.size() != size) {
            throw new Exception("Дубликаты в коде");
        }

        return resultCards;

    }

    @Test
    public void testDuplicates() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.SIX),
                        new Card(CardType.HEARTS, CardValue.JACK)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.FOUR),
                        new Card(CardType.DIAMONDS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.SPADES, CardValue.FOUR),
                        new Card(CardType.DIAMONDS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );


        Set<Card> cards = joinCollections(person.getCards(), commonCards2);

        Assert.assertTrue(!cards.isEmpty());
    }

    private Set<Card> getFlushSet(Set<Card> cards) {

        Map<CardType, Set<Card>> map = new HashMap<>();

        for (Card card : cards) {
            Set<Card> cardsByType = map.get(card.getType());

            if (cardsByType == null) {
                cardsByType = new HashSet<>();
            }

            cardsByType.add(card);

            map.put(card.getType(), cardsByType);
        }

        for (Map.Entry entry : map.entrySet()) {
            Set<Card> resultSet = map.get(entry.getKey());
            if (resultSet.size() > 4) {
                return resultSet;
            }
        }

        return new HashSet<>();
    }

    @Test
    public void straight() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.FOUR),
                        new Card(CardType.HEARTS, CardValue.TEN)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.SIX),
                        new Card(CardType.HEARTS, CardValue.SEVEN),
                        new Card(CardType.SPADES, CardValue.EIGHT),
                        new Card(CardType.HEARTS, CardValue.NINE),
                        new Card(CardType.CLUBS, CardValue.QUEEN)
                )
        );


        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.SIX),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.SPADES, CardValue.EIGHT),
                        new Card(CardType.HEARTS, CardValue.NINE),
                        new Card(CardType.CLUBS, CardValue.QUEEN)
                )
        );

        Set<Card> result = joinCollections(person.getCards(), commonCards);

        Card card = getTopStraightCard(result);

        Assert.assertNotNull(card);

        Assert.assertEquals(card, new Card(CardType.HEARTS, CardValue.TEN));

        result = joinCollections(person.getCards(), commonCards2);

        Assert.assertNull(getTopStraightCard(result));


    }

    private Card getTopStraightCard(Set<Card> result) {

        Set<Card> treeSet = new TreeSet<>(result);

        List<Card> list = new ArrayList<>(treeSet);

        int i = 0;

        int position = 0;

        for (int j = 0; j < list.size(); j++) {
            if (j != 0) {
                if (list.get(j).getValue().ordinal()
                        - list.get(j - 1).getValue().ordinal() == 1) {
                    i++;

                    if (i >= 4) {
                        position = j;
                    }

                } else {
                    i = 0;
                }
            }
        }

        Card card = null;

        if (position > 0) {
            card = list.get(position);
        }

        return card;
    }


    @Test
    public void straightFlush() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.HEARTS, CardValue.FOUR),
                        new Card(CardType.DIAMONDS, CardValue.TEN)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.SIX),
                        new Card(CardType.DIAMONDS, CardValue.SEVEN),
                        new Card(CardType.DIAMONDS, CardValue.EIGHT),
                        new Card(CardType.DIAMONDS, CardValue.NINE),
                        new Card(CardType.CLUBS, CardValue.QUEEN)
                )
        );


        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.SIX),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.SPADES, CardValue.EIGHT),
                        new Card(CardType.HEARTS, CardValue.NINE),
                        new Card(CardType.CLUBS, CardValue.QUEEN)
                )
        );

        Set<Card> commonCards3 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.SIX),
                        new Card(CardType.DIAMONDS, CardValue.SEVEN),
                        new Card(CardType.DIAMONDS, CardValue.EIGHT),
                        new Card(CardType.SPADES, CardValue.NINE),
                        new Card(CardType.CLUBS, CardValue.QUEEN)
                )
        );

        Set<Card> result = joinCollections(person.getCards(), commonCards);

        Assert.assertTrue(isStraightFlush(result));

        result = joinCollections(person.getCards(), commonCards2);


        Assert.assertFalse(isStraightFlush(result));

        result = joinCollections(person.getCards(), commonCards3);

        if (result.isEmpty()) {
            System.out.println("Дубликаты в колоде");
            System.exit(1);
        }

        Assert.assertFalse(isStraightFlush(result));

    }

    private boolean isStraightFlush(Set<Card> cards) {
        Set<Card> flushCards = getFlushSet(cards);

        boolean result = false;

        if (!flushCards.isEmpty()) {
            result = getTopStraightCard(flushCards) != null;
        }

        return result;
    }

    @Test
    public void quads() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.SPADES, CardValue.JACK),
                        new Card(CardType.CLUBS, CardValue.JACK)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.JACK),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> cards = joinCollections(person.getCards(), commonCards);

        CardValue value = getCardIfQuadsExist(cards);

        Assert.assertNotNull(value);

        Set<Card> cards2 = joinCollections(person.getCards(), commonCards2);

        CardValue value2 = getCardIfQuadsExist(cards2);

        Assert.assertNull(value2);
    }

    private CardValue getCardIfQuadsExist(Set<Card> cards) {
        return getCardValueBySize(cards, 4);
    }

    private CardValue getCardValueBySize(Set<Card> cards, Integer size) {
        Map<CardValue, Integer> cardMap = new HashMap<>();
        for (Card card : cards) {
            Integer count = cardMap.get(card.getValue());
            if (count == null) {
                cardMap.put(card.getValue(), 1);
            } else {
                cardMap.put(card.getValue(), count + 1);
            }
        }

        CardValue value = null;

        for (Map.Entry entry : cardMap.entrySet()) {
            if ((Integer) entry.getValue() >= size) {
                if (value == null) {
                    value = (CardValue) entry.getKey();
                }
            }
        }

        return value;
    }

    @Test
    public void fullHouse() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.SPADES, CardValue.JACK),
                        new Card(CardType.CLUBS, CardValue.TEN)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.JACK),
                        new Card(CardType.SPADES, CardValue.TEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.NINE),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> cards = joinCollections(person.getCards(), commonCards);

        Set<Card> fullHouse = getFullHouse(cards);

        Assert.assertTrue(!fullHouse.isEmpty());

        Set<Card> cards2 = joinCollections(person.getCards(), commonCards2);

        Set<Card> notFullHouse = getFullHouse(cards2);

        Assert.assertTrue(notFullHouse.isEmpty());

    }

    private Set<Card> getFullHouse(Set<Card> cards) {

        Set<Card> resultSet = new HashSet<>();

        Set<Card> threeOfKind = getThreeOfKind(cards);

        if (!threeOfKind.isEmpty()) {
            resultSet = removeSetAndFindNewCombination(cards, threeOfKind);
        }

        return resultSet;
    }

    private Set<Card> removeSetAndFindNewCombination(Set<Card> cards, Set<Card> previousCombination) {

        Set<Card> resultSet = new HashSet<>();

        Set<Card> outCards = new HashSet<>(cards);

        outCards.removeAll(previousCombination);

        Set<Card> pair = getPair(outCards);


        if (!pair.isEmpty()) {
            resultSet.addAll(pair);
            resultSet.addAll(previousCombination);
        }

        return resultSet;
    }

    private Set<Card> getPair(Set<Card> cards) {
        return getCardsBySize(cards, 2);
    }

    /**
     * Метод проверки на комбинацию СЕТ
     *
     * @param cards
     * @return
     */
    private Set<Card> getThreeOfKind(Set<Card> cards) {
        return getCardsBySize(cards, 3);
    }

    private Set<Card> getCardsBySize(Set<Card> cards, int size) {
        CardValue value = getCardValueBySize(cards, size);
        Set<Card> set = new HashSet<>();

        if (value != null) {

            set = cards
                    .stream()
                    .filter(item -> item.getValue().equals(value))
                    .collect(Collectors.toSet());
        }

        return set;
    }

    @Test
    public void threeOfKindTest() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.SPADES, CardValue.JACK),
                        new Card(CardType.CLUBS, CardValue.TEN)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.JACK),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.NINE),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );

        Set<Card> cards = joinCollections(person.getCards(), commonCards);

        Set<Card> result = getThreeOfKind(cards);

        Assert.assertTrue(!result.isEmpty());

        cards = joinCollections(person.getCards(), commonCards2);
        result = getThreeOfKind(cards);

        Assert.assertTrue(result.isEmpty());

    }

    @Test
    public void pairTest() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.SPADES, CardValue.JACK),
                        new Card(CardType.CLUBS, CardValue.TEN)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.JACK),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.NINE)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.NINE),
                        new Card(CardType.HEARTS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.EIGHT)
                )
        );

        Set<Card> cards = joinCollections(person.getCards(), commonCards);

        Set<Card> result = getPair(cards);

        Assert.assertTrue(!result.isEmpty());

        cards = joinCollections(person.getCards(), commonCards2);
        result = getPair(cards);

        Assert.assertTrue(result.isEmpty());
    }


    @Test
    public void twoPairsTest() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.SPADES, CardValue.JACK),
                        new Card(CardType.CLUBS, CardValue.TEN)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.JACK),
                        new Card(CardType.SPADES, CardValue.TEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.EIGHT)
                )
        );

        Set<Card> commonCards2 = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.QUEEN),
                        new Card(CardType.SPADES, CardValue.NINE),
                        new Card(CardType.HEARTS, CardValue.EIGHT),
                        new Card(CardType.DIAMONDS, CardValue.JACK)
                )
        );


        Set<Card> cards = joinCollections(person.getCards(), commonCards);

        Set<Card> result = getTwoPairs(cards);

        Assert.assertTrue(!result.isEmpty());

        cards = joinCollections(person.getCards(), commonCards2);
        result = getTwoPairs(cards);

        Assert.assertTrue(result.isEmpty());

    }

    private Set<Card> getTwoPairs(Set<Card> cards) {
        Set<Card> resultSet = new HashSet<>();
        Set<Card> firstPair = getPair(cards);

        if (!firstPair.isEmpty()) {
            resultSet = removeSetAndFindNewCombination(cards, firstPair);
        }

        return resultSet;
    }

    @Test
    public void topCardTest() throws Exception {
        Person person = new Person();
        Set<Card> set = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.SPADES, CardValue.JACK),
                        new Card(CardType.CLUBS, CardValue.TEN)
                )
        );


        person.setCards(set);

        Set<Card> commonCards = new HashSet<>(
                Arrays.asList(
                        new Card(CardType.DIAMONDS, CardValue.THREE),
                        new Card(CardType.HEARTS, CardValue.JACK),
                        new Card(CardType.SPADES, CardValue.QUEEN),
                        new Card(CardType.DIAMONDS, CardValue.KING),
                        new Card(CardType.DIAMONDS, CardValue.NINE)
                )
        );

        Set<Card> cards = joinCollections(person.getCards(), commonCards);

        Card king = getTopCard(cards);

        Assert.assertEquals(king, new Card(CardType.DIAMONDS, CardValue.KING));

        Assert.assertNotEquals(king, new Card(CardType.DIAMONDS, CardValue.NINE));
    }

    private Card getTopCard(Set<Card> cards) {
        return new TreeSet<>(cards).last();
    }
}
